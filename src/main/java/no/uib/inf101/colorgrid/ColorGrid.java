package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

//TODO save the cellpositions and colors to be able to return it

public class ColorGrid implements IColorGrid {

	//Field Variables
	int rows;
	int cols;
	
	
	List<CellColor>cells = new ArrayList<CellColor>();
	
	//Colorgrid gets the number of rows and columns
	public ColorGrid(int rows, int cols) {
		
		this.rows = rows;
		this.cols = cols;
		
		for (int i = 0; i < rows; i ++) {
			for (int j=0; j< cols; j++) {
				cells.add(new CellColor(new CellPosition(i, j), null));
			}
		}
	} 
	
	//return row number
	@Override
	public int rows() {
		return rows;
	}

	//return column number
	@Override
	public int cols() {
		return cols;
	}

	
	@Override
	public List<CellColor> getCells() {
		
		
		return cells;
	}

	//return the color if the set position equals the get position, otherwise return null
	@Override
	public Color get(CellPosition pos) {
		if (pos.row() < 0 || pos.row() >= ColorGrid.this.rows() || pos.col() < 0 || pos.col() >= ColorGrid.this.cols()) {
			throw new IndexOutOfBoundsException();
		} else {
		int index = (pos.row() * ColorGrid.this.cols() + pos.col()) ;
		//System.out.println(cells.get(index).color());
		
		return cells.get(index).color();
		}
		
	}
 
	//set the color and cell position
	@Override
	public void set(CellPosition pos, Color color) {
		if (pos.row() < 0 || pos.row() >= ColorGrid.this.rows() || pos.col() < 0 || pos.col() >= ColorGrid.this.cols()) {
			throw new IndexOutOfBoundsException();
		} else {
		int index = (pos.row() * ColorGrid.this.cols() + pos.col()) ;
		cells.set(index, (new CellColor(new CellPosition(pos.row(), pos.col()), color)));
		}
		//System.out.println(cells);
		//System.out.println(cells.get(index));
		//System.out.println(cells.get(index).color());
		//System.out.println(color);
		//System.out.println("row: " + pos.row());
		//System.out.println("col: " + pos.col());
		//System.out.println("INDEX " + index);
	//	System.out.println(cells.get(pos.col())) ;
		
	}
	
}
