package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // TODO: Implement this class
	//Box describes the area within which the grid should be shown
	 Rectangle2D box;
	//Gd describes the size of the grid the squares will be part of
	 GridDimension gd;
	//Margin describes how big the distance should be between the grids
	double margin;

	public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
		this.box = box;
		this.gd = gd;
		this.margin = margin;
		
	}
	//Responsible for calculating the position of one square in the grid.
	//Class for this helper method: CellPositionToPixelConverter
	public Rectangle2D getBoundsForCell(CellPosition cp) {
		//returns a Rectangle2D object
		double boxWidth = box.getWidth();
		double boxHeight = box.getHeight();
		double boxX = box.getX();
		double boxY = box.getY();
		double cpCol = cp.col();
		double cpRow = cp.row();
		double gdCol = gd.cols();
		double gdRow = gd.rows();
		
			
		double cellWidth = (boxWidth - ((gdCol +1) * margin)) / gdCol; //47.5 pixels
		double cellX = boxX + margin + ((margin + cellWidth) * cpCol); //215
		
		double cellHeight = (boxHeight -((gdRow + 1) * margin))/gdRow; // 40
		double cellY = boxY + margin + ((margin + cellHeight) * cpRow); //130
		
		Rectangle2D cell =  new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
		
		return cell;
		}
}

