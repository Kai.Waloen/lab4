package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    // TODO: Implement this method
	  	ColorGrid colorGrid = new ColorGrid(3, 4);
	  	GridView canvas = new GridView(colorGrid);
	    JFrame frame = new JFrame();
	    frame.setTitle("INF101");
	    frame.setContentPane(canvas);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.pack();
	    frame.setVisible(true);
	   
	   
	    
	    colorGrid.set(new CellPosition(0, 0), Color.RED);
	    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
	    colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
	    colorGrid.set(new CellPosition(2, 3), Color.GREEN);
	   
  }
  }
	    
	    
	    
	    
