package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
	IColorGrid colorgrid;	
	
private static final double OUTERMARGIN = 30;
private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
	
	public GridView(IColorGrid colorgrid) {
	    this.setPreferredSize(new Dimension(400, 300));
	    this.colorgrid = colorgrid; 
	    
	   
	  }
	
	  @Override
	  public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    Graphics2D g2= (Graphics2D) g; 
	    drawGrid(g2);
	    
	    
	  }
	  	  
	  //Responsible for drawing a complete grid, including frames and squares 
	  //(everything within the gray area).
	  public void drawGrid(Graphics2D graphics2D) {

		  double x = OUTERMARGIN;
		  double y = OUTERMARGIN;
		  double width = this.getWidth() - 2 * OUTERMARGIN;
		  double height = this.getHeight() - 2 * OUTERMARGIN;
		  graphics2D.setColor(MARGINCOLOR);
		  graphics2D.fill(new Rectangle2D.Double(x, y, width, height));
		  CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(getBounds(), colorgrid, OUTERMARGIN);
		  drawCells(graphics2D, colorgrid, cellPositionToPixelConverter);
	  }
	  
	  //Responsible for drawing a collection of grids. For each grid, this method
	  //calculates where the grid should be by calling the helper method.
	  private static void drawCells(Graphics2D graphics2D, CellColorCollection collection, CellPositionToPixelConverter converter) {
		  List<CellColor> list = collection.getCells();
		  for (int i = 0; i < list.size(); i++) {
			 Rectangle2D rectangle2D = converter.getBoundsForCell(list.get(i).cellPosition());
			 Color color = list.get(i).color();
			 if (color == null) {
				 color = Color.DARK_GRAY;
				 
			 }
			 graphics2D.setColor(color);
			 graphics2D.fill(rectangle2D);
		  }
		  
		  
		  
	  
	  }  
}